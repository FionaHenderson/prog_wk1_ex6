﻿using System;

namespace Exercise6
{
    class MainClass
    {
        static void Main(string[] args)
        {
            Console.WriteLine("How many kilometers did you drive?");
            var input = double.Parse(Console.ReadLine());

            const double kmstomiles = 0.621371;
            var answer = Math.Round(input * kmstomiles);

            Console.WriteLine($"You travelled {input} kms which converts to approximatley {answer} miles");
            
        }
    }
}
